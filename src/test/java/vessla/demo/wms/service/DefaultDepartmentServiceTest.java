package vessla.demo.wms.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import vessla.demo.wms.dao.CurrencyRepository;
import vessla.demo.wms.dao.DepartmentRepository;
import vessla.demo.wms.dao.OrderItemRepository;
import vessla.demo.wms.dao.OrderRepository;
import vessla.demo.wms.model.Department;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(/*webEnvironment=WebEnvironment.RANDOM_PORT*/)
@TestPropertySource(locations = "classpath:application-test.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class DefaultDepartmentServiceTest {
    /* TIPS:
     * For tests of the JPA layer with a separate (in-memory) database
     * (assuming maven dependency for the supported embedded database in pom.xml)
     * use the annotations below:
     * @RunWith(SpringRunner.class)
     * @DataJpaTest
     *
     * Not using this feature now, because of different results of constraint violation test
     * in comparison to the PostgreSQL database that will be used "in production" (other differences possible).
     */

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private DefaultDepartmentService departmentService;

    @Before
    public void prepareDatabaseContents() throws Exception {
        Department department = new Department("CNC");
        departmentRepository.save(department);
    }

    @Test
    public void testSaveExistingDepartmentNameAsNew() {
        Department department = new Department(100, "CNC");

        SaveResult<Department, Department> saveResult = departmentService.saveDepartment(department);
        //insert should fail - department with a given name already exists
        assertNull(saveResult.getSavedEntity());
        assertEquals(new Integer(1), saveResult.getConflictingEntity().getId());

        //checking if no extra objects have been saved
        assertEquals(1, departmentRepository.findAll().spliterator().getExactSizeIfKnown());
    }

    @Test
    @DirtiesContext
    public void testInsertNewDepartment() {
        Department newDepartment = new Department(1, "Gulgulacje");

        SaveResult<Department, Department> saveResult = departmentService.saveDepartment(newDepartment);

        //insert should succeed
        assertNotNull(saveResult.getSavedEntity());
        assertEquals(new Integer(2), saveResult.getSavedEntity().getId());
        assertEquals("Gulgulacje", saveResult.getSavedEntity().getName());

        //checking if extra object has been persisted correctly
        assertEquals(2, departmentRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals("Gulgulacje", departmentRepository.findById(2).get().getName());

        //checking if the original object has not been modified
        assertEquals("CNC", departmentRepository.findById(1).get().getName());
    }

    @After
    public void clearDatabase() {
        //taken care of by Hibernate ddl-auto=create-drop and @DirtiesContext for insert/update tests (Flyway is disabled for tests)
    }

}
