package vessla.demo.wms.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import vessla.demo.wms.dao.CurrencyRepository;
import vessla.demo.wms.dao.DepartmentRepository;
import vessla.demo.wms.dao.OrderItemRepository;
import vessla.demo.wms.dao.OrderRepository;
import vessla.demo.wms.model.Currency;
import vessla.demo.wms.model.Department;
import vessla.demo.wms.model.Order;
import vessla.demo.wms.model.OrderItem;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(/*webEnvironment=WebEnvironment.RANDOM_PORT*/)
@TestPropertySource(locations = "classpath:application-test.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DefaultOrderServiceTest {
    /* TIPS:
     * For tests of the JPA layer with a separate (in-memory) database
     * (assuming maven dependency for the supported embedded database in pom.xml)
     * use the annotations below:
     * @RunWith(SpringRunner.class)
     * @DataJpaTest
     *
     * Not using this feature now, because of different results of constraint violation test
     * in comparison to the PostgreSQL database that will be used "in production" (other differences possible).
     */

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private DefaultOrderService orderService;

    @Before
    public void prepareDatabaseContents() throws Exception {
        Currency currency = currencyRepository.save(new Currency(1, "EUR"));
        OrderItem item1 = new OrderItem(1L, "gulgulator", 2, 2.50f, currency);
        OrderItem item2 = new OrderItem(2L, "łożyska", 1, 3.50f, currency);

        Department department = departmentRepository.save(new Department("CNC"));
        Order newOrder = new Order(1L, LocalDateTime.now(), "my fancy description", department);
        newOrder.addItem(item1);
        newOrder.addItem(item2);

        orderRepository.save(newOrder);
    }

    @Test
    public void testSaveExistingOrderIdAsNew() {
        OrderItem item1 = new OrderItem(100L, "ephl synthether", 2, 2.50f, currencyRepository.findById(1).get());
        OrderItem item2 = new OrderItem(101L, "buty w sprayu", 1, 3.50f, currencyRepository.findById(1).get());

        Order newOrder = new Order(1L, LocalDateTime.now(), "my modified description", departmentRepository.findById(1).get());
        newOrder.addItem(item1);
        newOrder.addItem(item2);

        //insert should fail - it is not an update request
        assertEquals(null, orderService.saveOrder(newOrder, false).getSavedEntity());
        assertEquals(new Long(1), orderService.saveOrder(newOrder, false).getConflictingEntity().getId());
        assertEquals("my fancy description", orderService.saveOrder(newOrder, false).getConflictingEntity().getDescription());

        //checking if no extra objects have been saved
        assertEquals(1, departmentRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(1, currencyRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(1, orderRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(2, orderItemRepository.findAll().spliterator().getExactSizeIfKnown());

        //checking if the original object has not been modified
        assertEquals("my fancy description", orderRepository.findById(1L).get().getDescription());

        //checking if the original order items have not been modified
        assertEquals("gulgulator", orderItemRepository.findById(1L).get().getName());
        assertEquals("łożyska", orderItemRepository.findById(2L).get().getName());
    }

    @Test
    public void testSaveOrderWithExistingItemIdAsNew() {
        OrderItem item1 = new OrderItem(5L, "ephl synthether", 2, 2.50f, currencyRepository.findById(1).get());
        OrderItem item2 = new OrderItem(1L, "buty w sprayu", 1, 3.50f, currencyRepository.findById(1).get());

        Order newOrder = new Order(100L, LocalDateTime.now(), "my modified description", departmentRepository.findById(1).get());
        newOrder.addItem(item1);
        newOrder.addItem(item2);

        //insert should fail - it is not an update request
        SaveResult<Order, Order> saveResult = orderService.saveOrder(newOrder, false);
        assertEquals(null, saveResult.getSavedEntity());
        assertEquals(new Long(1), saveResult.getConflictingEntity().getId());
        assertEquals("my fancy description", saveResult.getConflictingEntity().getDescription());

        //checking if no extra objects have been saved
        assertEquals(1, departmentRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(1, currencyRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(1, orderRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(2, orderItemRepository.findAll().spliterator().getExactSizeIfKnown());

        //checking if the original object has not been modified
        assertEquals("my fancy description", orderRepository.findById(1L).get().getDescription());

        //checking if the original order items have not been modified
        assertEquals("gulgulator", orderItemRepository.findById(1L).get().getName());
        assertEquals("łożyska", orderItemRepository.findById(2L).get().getName());
    }

    @Test
    public void testUpdateExistingOrderDescriptionDateAndItem() {
        Currency currency = new Currency("SEK");
        OrderItem item1 = new OrderItem(1L, "ephl synthether", 2, 2.50f, currency);
        OrderItem item2 = new OrderItem(2L, "łożyska", 1, 3.50f, currencyRepository.findById(1).get());

        Order newOrder = new Order(1L, LocalDateTime.now(), "my modified description", departmentRepository.findById(1).get());
        newOrder.addItem(item1);
        newOrder.addItem(item2);

        //insert should succeed - it is an update request
        assertNotNull(orderService.saveOrder(newOrder, true).getSavedEntity());

        //checking if no extra objects have been saved
        assertEquals(1, departmentRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(2, currencyRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(1, orderRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(2, orderItemRepository.findAll().spliterator().getExactSizeIfKnown());

        //checking if the original object has been modified correctly
        assertEquals("my modified description", orderRepository.findById(1L).get().getDescription());

        //checking if the original order items have been modified correctly
        assertEquals("ephl synthether", orderItemRepository.findById(1L).get().getName());
        assertEquals("SEK", orderItemRepository.findById(1L).get().getCurrency().getName());
        assertEquals("łożyska", orderItemRepository.findById(2L).get().getName());
    }

    @Test
    public void testInsertNewOrder() {
        Currency currency = new Currency(2, "SEK");
        OrderItem item = new OrderItem(-1L, "łożyska kulkowe", 1, 3.50f, currency/*currencyRepository.findById(1).get()*/);
        OrderItem item1 = new OrderItem(-1L, "gulgulator", 1, 3.50f, currency/*currencyRepository.findById(1).get()*/);

        Order newOrder = new Order(-1L, LocalDateTime.now(), "my brand new description", departmentRepository.findById(1).get());
        newOrder.addItem(item);
        newOrder.addItem(item1);

        //insert should succeed - it is an update request
        SaveResult<Order, Order> saveResult = orderService.saveOrder(newOrder, false);
        assertNotNull(saveResult.getSavedEntity());
        assertNull(saveResult.getConflictingEntity());

        //checking if no extra objects have been saved
        assertEquals(1, departmentRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(2, currencyRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(2, orderRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(4, orderItemRepository.findAll().spliterator().getExactSizeIfKnown());

        //checking if the original object has not been modified
        assertEquals("my fancy description", orderRepository.findById(1L).get().getDescription());

        //checking if the original order items have not been modified
        assertEquals("gulgulator", orderItemRepository.findById(1L).get().getName());
        assertEquals("łożyska", orderItemRepository.findById(2L).get().getName());

        //checking if the new order and its items has been saved correctly
        Order persistedOrder = orderRepository.findById(2L).get();
        assertEquals("my brand new description", persistedOrder.getDescription());
        assertEquals("łożyska kulkowe", orderItemRepository.findById(3L).get().getName());
    }

    @Test
    public void testDeleteOrder() {
        orderService.deleteOrder(1L);

        //checking if no extra objects have been saved
        assertEquals(1, departmentRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(1, currencyRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(0, orderRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals(0, orderItemRepository.findAll().spliterator().getExactSizeIfKnown());
    }

    @After
    public void clearDatabase() {
        //taken care of by Hibernate ddl-auto=create-drop and @DirtiesContext for insert/update tests (Flyway is disabled for tests)
    }

}
