package vessla.demo.wms.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        DefaultCurrencyServiceTest.class,
        DefaultDepartmentServiceTest.class,
        DefaultOrderServiceTest.class
})
public class ServicesTestSuite {
}
