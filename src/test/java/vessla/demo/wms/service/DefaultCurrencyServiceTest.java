package vessla.demo.wms.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import vessla.demo.wms.dao.CurrencyRepository;
import vessla.demo.wms.model.Currency;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(/*webEnvironment=WebEnvironment.RANDOM_PORT*/)
@TestPropertySource(locations = "classpath:application-test.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DefaultCurrencyServiceTest {
    /* TIPS:
     * For tests of the JPA layer with a separate (in-memory) database
     * (assuming maven dependency for the supported embedded database in pom.xml)
     * use the annotations below:
     * @RunWith(SpringRunner.class)
     * @DataJpaTest
     *
     * Not using this feature now, because of different results of constraint violation test
     * in comparison to the PostgreSQL database that will be used "in production" (other differences possible).
     */

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private DefaultCurrencyService currencyService;

    @Before
    public void prepareDatabaseContents() throws Exception {
        Currency currency = new Currency("EUR");
        currencyRepository.save(currency);
    }

    @Test
    public void testSaveExistingCurrencyNameAsNew() {
        Currency newCurrency = new Currency(100, "EUR");

        SaveResult<Currency, Currency> saveResult = currencyService.saveCurrency(newCurrency);

        //insert should fail - currency with a given name already exists
        assertNull(saveResult.getSavedEntity());
        assertEquals(new Integer(1), saveResult.getConflictingEntity().getId());

        //checking if no extra objects have been saved
        assertEquals(1, currencyRepository.findAll().spliterator().getExactSizeIfKnown());
    }

    @Test
    public void testInsertNewDepartment() {
        Currency newCurrency = new Currency(1, "SEK");

        SaveResult<Currency, Currency> saveResult = currencyService.saveCurrency(newCurrency);
        //insert should succeed
        assertNotNull(saveResult.getSavedEntity());
        assertEquals(new Integer(2), saveResult.getSavedEntity().getId());

        //checking if extra object has been persisted correctly
        assertEquals(2, currencyRepository.findAll().spliterator().getExactSizeIfKnown());
        assertEquals("SEK", currencyRepository.findById(2).get().getName());

        //checking if the original object has not been modified
        assertEquals("EUR", currencyRepository.findById(1).get().getName());
    }

    @After
    public void clearDatabase() {
        //taken care of by Hibernate ddl-auto=create-drop and @DirtiesContext for insert/update tests (Flyway is disabled for tests)
    }

}
