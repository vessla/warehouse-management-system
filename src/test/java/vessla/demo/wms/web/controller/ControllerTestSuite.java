package vessla.demo.wms.web.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        CurrencyControllerTests.class,
        DepartmentControllerTests.class,
        OrderControllerTests.class
})
public class ControllerTestSuite {
}
