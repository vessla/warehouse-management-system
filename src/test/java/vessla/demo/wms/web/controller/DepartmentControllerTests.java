package vessla.demo.wms.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.client.Traverson;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import vessla.demo.wms.model.Department;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:application-test.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DepartmentControllerTests {

    @LocalServerPort
    private int port;

    @Before
    public void addSampleDepartment(){
        Department newDepartment = new Department(1, "CNC");
        new TestRestTemplate().postForLocation("http://localhost:"+port+"/departments", newDepartment);
    }

    @Test
    public void shouldReturnOneDepartment() throws URISyntaxException {
        Traverson traverson = new Traverson(new URI("http://localhost:"+port+"/departments/1"), MediaTypes.HAL_JSON);
        Traverson.TraversalBuilder traversonBuilder = traverson.follow();
        ParameterizedTypeReference<Resource<Department>> typeReference = new ParameterizedTypeReference<Resource<Department>>(){};
        Resource<Department> retrivedResource = traversonBuilder.toObject(typeReference);

        assertEquals("CNC", retrivedResource.getContent().getName());
        //checking URL instead of id, due to lack of deserialization of an id property (shadowed by Resource property?)
        //(https://github.com/spring-projects/spring-hateoas/issues/67
        assertEquals("http://localhost:"+port+"/departments/1", retrivedResource.getLinks("self").get(0).getHref());
    }

    @Test
    public void shouldReturnOneDepartmentInCollection() throws URISyntaxException {
        //using Traverson, because RestTemplate seems to have problems with collection deserialization (returns empty collection)
        Traverson traverson = new Traverson(new URI("http://localhost:"+port+"/departments"), MediaTypes.HAL_JSON);
        Traverson.TraversalBuilder traversonBuilder = traverson.follow();
        ParameterizedTypeReference<Resources<Resource<Department>>> typeReference = new ParameterizedTypeReference<Resources<Resource<Department>>>() {};
        Resources<Resource<Department>> resourcesDepartments = traversonBuilder.toObject(typeReference);
        Collection<Resource<Department>> retrivedDepartments = resourcesDepartments.getContent();

        assertEquals(1, retrivedDepartments.size());
        Department retrivedDepartment = retrivedDepartments.iterator().next().getContent();
        assertEquals("CNC", retrivedDepartment.getName());
        //checking URL instead of id, due to lack of deserialization of an id property (shadowed by Resource property?)
        //(https://github.com/spring-projects/spring-hateoas/issues/67
        assertEquals("http://localhost:"+port+"/departments/1", retrivedDepartments.iterator().next().getLinks("self").get(0).getHref());
        assertEquals("http://localhost:"+port+"/departments", resourcesDepartments.getLinks("self").get(0).getHref());
    }
}
