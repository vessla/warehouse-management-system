package vessla.demo.wms.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.*;
import org.springframework.hateoas.client.Traverson;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import vessla.demo.wms.model.Currency;
import vessla.demo.wms.model.Department;
import vessla.demo.wms.model.Order;
import vessla.demo.wms.model.OrderItem;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:application-test.properties")
public class OrderControllerTests {

    @LocalServerPort
    private int port;

    @Test
    public void shouldReturnOneOrderInCollection() throws URISyntaxException {
        Currency currency = new Currency(1, "EUR");
        OrderItem item1 = new OrderItem(1L, "gulgulator", 2, 2.50f, currency);
        OrderItem item2 = new OrderItem(2L, "łożyska", 1, 3.50f, currency);

        Department department = new Department("CNC");
        Order newOrder = new Order(1L, LocalDateTime.now(), "my fancy description", department);
        newOrder.addItem(item1);
        newOrder.addItem(item2);

        new TestRestTemplate().postForLocation("http://localhost:"+port+"/orders", newOrder);

        //using Traverson, because RestTemplate seems to have problems with collection deserialization (returns empty collection)
        Traverson traverson = new Traverson(new URI("http://localhost:"+port+"/orders"), MediaTypes.HAL_JSON);
        Traverson.TraversalBuilder traversonBuilder = traverson.follow();
        ParameterizedTypeReference<PagedResources<Resource<Order>>> typeReference = new ParameterizedTypeReference<PagedResources<Resource<Order>>>(){};
        PagedResources<Resource<Order>> resourcesOrders = traversonBuilder.toObject(typeReference);
        Collection<Resource<Order>> retrivedOrders = resourcesOrders.getContent();

        assertEquals(1, retrivedOrders.size());
        Order retrivedOrder = retrivedOrders.iterator().next().getContent();
        assertEquals("my fancy description", retrivedOrder.getDescription());
        //checking URL instead of id, due to lack of deserialization of an id property (shadowed by Resource property?)
        //(https://github.com/spring-projects/spring-hateoas/issues/67
        assertEquals("http://localhost:"+port+"/orders/1", retrivedOrders.iterator().next().getLinks("self").get(0).getHref());
        assertEquals("http://localhost:"+port+"/orders", resourcesOrders.getLinks("self").get(0).getHref());
    }

}
