package vessla.demo.wms.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.client.Traverson;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import vessla.demo.wms.model.Currency;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:application-test.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class CurrencyControllerTests {

    @LocalServerPort
    private int port;

    @Before
    public void addSampleCurrency(){
        Currency newCurrency = new Currency(1, "SEK");
        new TestRestTemplate().postForLocation("http://localhost:"+port+"/currencies", newCurrency);
    }

    @Test
    public void shouldReturnOneCurrency() throws URISyntaxException {
        Traverson traverson = new Traverson(new URI("http://localhost:"+port+"/currencies/1"), MediaTypes.HAL_JSON);
        Traverson.TraversalBuilder traversonBuilder = traverson.follow();
        ParameterizedTypeReference<Resource<Currency>> typeReference = new ParameterizedTypeReference<Resource<Currency>>(){};
        Resource<Currency> retrievedResource = traversonBuilder.toObject(typeReference);

        assertEquals("SEK", retrievedResource.getContent().getName());
        //checking URL instead of id, due to lack of deserialization of an id property (shadowed by Resource property?)
        //(https://github.com/spring-projects/spring-hateoas/issues/67
        assertEquals("http://localhost:"+port+"/currencies/1", retrievedResource.getLinks("self").get(0).getHref());

        /* Somehow TestRestTemplate does not deserialize links correctly in this case (returns empty collection) - using Traverson instead

        ResponseEntity<Resource<Currency>> responseEntity = restTemplate.exchange(currencyURI, HttpMethod.GET, null, new ParameterizedTypeReference<Resource<Currency>>(){});

        Resource<Currency> retrievedResource = responseEntity.getBody();
        assertEquals("SEK", retrievedResource.getContent().getName());
        //checking URL instead of id, due to lack of deserialization of an id property (shadowed by Resource property?)
        //(https://github.com/spring-projects/spring-hateoas/issues/67
        assertEquals("http://localhost:"+port+"/currencies/1", retrievedResource.getLinks("self").get(0).getHref());
        */
    }

    @Test
    public void shouldReturnOneCurrencyInCollection() throws URISyntaxException {
        //using Traverson, because RestTemplate seems to have problems with collection deserialization (returns empty collection)
        Traverson traverson = new Traverson(new URI("http://localhost:"+port+"/currencies"), MediaTypes.HAL_JSON);
        Traverson.TraversalBuilder traversonBuilder = traverson.follow();
        ParameterizedTypeReference<Resources<Resource<Currency>>> typeReference = new ParameterizedTypeReference<Resources<Resource<Currency>>>(){};
        Resources<Resource<Currency>> resourcesCurrencies = traversonBuilder.toObject(typeReference);
        Collection<Resource<Currency>> retrivedCurrencies = resourcesCurrencies.getContent();

        assertEquals(1, retrivedCurrencies.size());
        Currency retrivedCurrency = retrivedCurrencies.iterator().next().getContent();
        assertEquals("SEK", retrivedCurrency.getName());
        //checking URL instead of id, due to lack of deserialization of an id property (shadowed by Resource property?)
        //(https://github.com/spring-projects/spring-hateoas/issues/67
        assertEquals("http://localhost:"+port+"/currencies/1", retrivedCurrencies.iterator().next().getLinks("self").get(0).getHref());
        assertEquals("http://localhost:"+port+"/currencies", resourcesCurrencies.getLinks("self").get(0).getHref());
    }
}
