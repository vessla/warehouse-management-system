package vessla.demo.wms.dao;

import org.springframework.data.repository.CrudRepository;
import vessla.demo.wms.model.OrderItem;

public interface OrderItemRepository extends CrudRepository<OrderItem, Long> {

}
