package vessla.demo.wms.dao;

import org.springframework.data.repository.CrudRepository;
import vessla.demo.wms.model.Department;

import java.util.List;

public interface DepartmentRepository extends CrudRepository<Department, Integer> {

    List<Department> findByName(String name);

}
