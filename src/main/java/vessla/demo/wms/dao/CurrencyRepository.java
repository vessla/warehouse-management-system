package vessla.demo.wms.dao;

import org.springframework.data.repository.CrudRepository;
import vessla.demo.wms.model.Currency;

import java.util.List;

public interface CurrencyRepository extends CrudRepository<Currency, Integer> {

    List<Currency> findByName(String name);

}
