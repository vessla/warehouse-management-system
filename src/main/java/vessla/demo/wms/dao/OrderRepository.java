package vessla.demo.wms.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.repository.PagingAndSortingRepository;
import vessla.demo.wms.model.Order;

import java.util.List;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long> {

    /*List*/Page<Order> findAllByOrderByIdDesc(Pageable pageable);

}
