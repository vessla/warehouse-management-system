package vessla.demo.wms.web.controller.hateoas;


import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;
import vessla.demo.wms.model.Currency;
import vessla.demo.wms.web.controller.CurrencyController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class CurrencyResourceAssembler implements ResourceAssembler<Currency, Resource<Currency>> {

    @Override
    public Resource<Currency> toResource(Currency currency) {

        return new Resource<>(currency,
                ControllerLinkBuilder.linkTo(methodOn(CurrencyController.class).readCurrency(currency.getId())).withSelfRel());
    }

}
