package vessla.demo.wms.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vessla.demo.wms.model.Currency;
import vessla.demo.wms.service.DefaultCurrencyService;
import vessla.demo.wms.service.SaveResult;
import vessla.demo.wms.web.controller.hateoas.CurrencyResourceAssembler;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/currencies")
public class CurrencyController {

    private final DefaultCurrencyService currencyService;

    private final CurrencyResourceAssembler resourceAssembler;

    @Autowired
    CurrencyController(DefaultCurrencyService currencyService, CurrencyResourceAssembler resourceAssembler) {
        this.currencyService = currencyService;
        this.resourceAssembler = resourceAssembler;
    }

    @PostMapping(produces = "application/hal+json;charset=utf-8")
    public ResponseEntity<Resource<Currency>> createCurrency(@RequestBody Currency currency){
        SaveResult<Currency, Currency> createResult = currencyService.saveCurrency(currency);
        if(createResult.getSavedEntity() != null){
            Resource<Currency> savedResource = resourceAssembler.toResource(createResult.getSavedEntity());
            try {
                return ResponseEntity.created(new URI(savedResource.getId().expand().getHref())).body(savedResource);
            } catch (URISyntaxException e) {
                return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else{
            return new ResponseEntity<>(resourceAssembler.toResource(createResult.getConflictingEntity()), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(produces = "application/hal+json;charset=utf-8")
    public ResponseEntity<Resources<Resource<Currency>>> readCurrencies(){
        List<Resource<Currency>> currencies = currencyService.getCurrencies().stream()
                .map(resourceAssembler::toResource)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(new Resources<>(currencies,
                linkTo(methodOn(CurrencyController.class).readCurrencies()).withSelfRel()));
    }

    @GetMapping(value = "/{id}", produces = "application/hal+json;charset=utf-8")
    public ResponseEntity<Resource<Currency>> readCurrency(@PathVariable Integer id) {
        Currency currency = currencyService.getCurrency(id);
        if(currency != null) {
            return ResponseEntity.ok().body(resourceAssembler.toResource(currency));
        }
        else{
            return ResponseEntity.noContent().build();
        }
    }

}
