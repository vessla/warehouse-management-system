package vessla.demo.wms.web.controller.hateoas;


import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;
import vessla.demo.wms.model.Department;
import vessla.demo.wms.web.controller.DepartmentController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class DepartmentResourceAssembler implements ResourceAssembler<Department, Resource<Department>> {

    @Override
    public Resource<Department> toResource(Department department) {

        return new Resource<>(department,
                ControllerLinkBuilder.linkTo(methodOn(DepartmentController.class).readDepartment(department.getId())).withSelfRel());
    }

}
