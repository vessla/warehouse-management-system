package vessla.demo.wms.web.controller;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import vessla.demo.wms.model.Order;
import vessla.demo.wms.service.DefaultOrderService;
import vessla.demo.wms.service.SaveResult;
import vessla.demo.wms.web.controller.hateoas.OrderResourceAssembler;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/*
 * Endpoint URI and HTTP methods mappings according to Allegro REST API guidelines:
 * https://github.com/allegro/restapi-guideline#use-http-methods-to-operate-on-collections-and-entities
 */

@RestController
@RequestMapping("/orders")
public class OrderController {

    private static Logger logger = LoggerFactory.getLogger(OrderController.class);

    private String defaultReportFolder;

    @Value( "${general.company.name}" )
    private String companyName;

    @Value( "${general.company.city}" )
    private String companyCity;

    @Value( "${general.company.logo}" )
    private String companyLogo;

    @Value( "${general.locale.language}" )
    private String localeLanguage;

    @Value( "${general.locale.country}" )
    private String localeCountry;


    private final DefaultOrderService orderService;
    private final OrderResourceAssembler resourceAssembler;

    @Autowired
    OrderController(DefaultOrderService orderService, OrderResourceAssembler resourceAssembler) {
        this.defaultReportFolder = "/reports";
        this.orderService = orderService;
        this.resourceAssembler = resourceAssembler;
    }

    @PostMapping(produces = "application/hal+json;charset=utf-8")
    public ResponseEntity<Resource<Order>> createOrder(@RequestBody Order order){
        SaveResult<Order, Order> createResult = orderService.saveOrder(order, false);
        if(createResult.getSavedEntity() != null){
            Resource<Order> savedResource = resourceAssembler.toResource(createResult.getSavedEntity());
            try {
               return ResponseEntity.created(new URI(savedResource.getId().expand().getHref())).body(savedResource);
            } catch (URISyntaxException e) {
                return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else{
            return new ResponseEntity<>(resourceAssembler.toResource(createResult.getConflictingEntity()), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(produces = "application/hal+json;charset=utf-8")
    public ResponseEntity<PagedResources<Resource<Order>>> readOrders(Pageable page){
        Page<Order> requestedPage = orderService.getOrders(page);
        List<Resource<Order>> orderResources = requestedPage.stream()
                .map(resourceAssembler::toResource)
                .collect(Collectors.toList());
        PagedResources.PageMetadata pageMetadata = new PagedResources.PageMetadata(requestedPage.getSize(), requestedPage.getNumber(), requestedPage.getTotalElements(), requestedPage.getTotalPages());
        PagedResources<Resource<Order>> pagedResources = new PagedResources<>(orderResources, pageMetadata,
                linkTo(methodOn(OrderController.class).readOrders(page)).withSelfRel());
        return ResponseEntity.ok().body(pagedResources);
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> generateSingleOrderPdf(@PathVariable Long id) {
        Order order = orderService.getOrder(id);
        if(order != null) {
            byte[] reportData = generateOrderPdf("singleOrderReport.jrxml", order);
            return ResponseEntity
                    .ok()
                    // Specify content type as PDF
                    .header("Content-Type", "application/pdf; charset=UTF-8")
                    // Tell browser to display PDF if it can
                    .header("Content-Disposition", "inline; filename=order_" + id + ".pdf")
                    .body(reportData);
        }
        else{
            return ResponseEntity.noContent().build();
        }
    }

    @PutMapping(value="/{id}", produces = "application/hal+json;charset=utf-8")
    public ResponseEntity<Resource<Order>> updateOrder(@RequestBody Order order){
        SaveResult<Order, Order> updateResult = orderService.saveOrder(order,false);
        if(updateResult.getSavedEntity() != null){
            return ResponseEntity.ok().body(resourceAssembler.toResource(updateResult.getSavedEntity()));
        }
        else{
            return ResponseEntity.noContent().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOrder(@PathVariable Long id){
        orderService.deleteOrder(id);
        return ResponseEntity.noContent().build();
    }

    public byte[] generateOrderPdf(String inputFileName, Order order) {
        byte[] reportData = null;

        try (ByteArrayOutputStream byteArray = new ByteArrayOutputStream()) {
            JasperReport jasperReport = null;
            Map<String, Object> reportParameters = new HashMap<>();
            reportParameters.put("department", order.getDepartment().getName());
            reportParameters.put("date", order.getCreationTime());
            reportParameters.put("description", order.getDescription());
            reportParameters.put("companyName", companyName);
            reportParameters.put("companyLogo", companyLogo);
            reportParameters.put("city", companyCity);
            /*Localized resource bundle for the report is not automatically loaded via <jasperReport resourceBundle="..."> in jrxml
            due to problems while trying to display polish characters with the default encoding*/
            reportParameters.put("locale", new Locale(localeLanguage, localeCountry));
            InputStream input = getClass().getResourceAsStream(defaultReportFolder+"/i18n_"+localeLanguage+"_"+localeCountry+".properties");
            Reader reader = new InputStreamReader(input, "UTF-8");
            ResourceBundle resourceBundle = new PropertyResourceBundle(reader);
            reportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, resourceBundle);

            // If a compiled report (.jasper) exists, load it - compile and save otherwise
            try{
                jasperReport = (JasperReport)JRLoader.loadObjectFromFile(inputFileName.replace(".jrxml",".jasper"));
                logger.error("Precompiled report successfully loaded");
            }
            catch (JRException e){
                logger.error("Could not load a precompiled report");
                InputStream reportTemplateStream = getClass().getResourceAsStream(defaultReportFolder+"/"+inputFileName);
                jasperReport = JasperCompileManager.compileReport(reportTemplateStream);
                JRSaver.saveObject(jasperReport, inputFileName.replace(".jrxml",".jasper"));
            }

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, reportParameters, new JRBeanCollectionDataSource(order.getItems()));
            // return the PDF in bytes
            reportData = JasperExportManager.exportReportToPdf(jasperPrint);
        }
        catch (JRException | IOException e) {
            e.printStackTrace();
        }
        return reportData;
    }

}
