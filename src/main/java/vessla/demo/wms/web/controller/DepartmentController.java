package vessla.demo.wms.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vessla.demo.wms.model.Department;
import vessla.demo.wms.service.DefaultDepartmentService;
import vessla.demo.wms.service.SaveResult;
import vessla.demo.wms.web.controller.hateoas.DepartmentResourceAssembler;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/departments")
public class DepartmentController {

    private final DefaultDepartmentService departmentService;

    private final DepartmentResourceAssembler resourceAssembler;

    @Autowired
    DepartmentController(DefaultDepartmentService departmentService, DepartmentResourceAssembler resourceAssembler) {
        this.departmentService = departmentService;
        this.resourceAssembler = resourceAssembler;
    }

    @PostMapping(produces = "application/json;charset=utf-8")
    public ResponseEntity<Resource<Department>> createDepartment(@RequestBody Department department){
        SaveResult<Department, Department> createResult = departmentService.saveDepartment(department);
        if(createResult.getSavedEntity() != null){
            Resource<Department> savedResource = resourceAssembler.toResource(createResult.getSavedEntity());
            try {
                return ResponseEntity.created(new URI(savedResource.getId().expand().getHref())).body(savedResource);
            } catch (URISyntaxException e) {
                return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else{
            return new ResponseEntity<>(resourceAssembler.toResource(createResult.getConflictingEntity()), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(produces = "application/hal+json;charset=utf-8")
    public ResponseEntity<Resources<Resource<Department>>> readDepartments(){
        List<Resource<Department>> departments = departmentService.getDepartments().stream()
                .map(resourceAssembler::toResource)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(new Resources<>(departments,
                linkTo(methodOn(DepartmentController.class).readDepartments()).withSelfRel()));
    }

    @GetMapping(value = "/{id}", produces = "application/hal+json;charset=utf-8")
    public ResponseEntity<Resource<Department>> readDepartment(@PathVariable Integer id) {
        Department department = departmentService.getDepartment(id);
        if(department != null) {
            return ResponseEntity.ok().body(resourceAssembler.toResource(department));
        }
        else{
            return ResponseEntity.noContent().build();
        }
    }

}
