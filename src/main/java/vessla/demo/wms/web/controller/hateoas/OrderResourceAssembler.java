package vessla.demo.wms.web.controller.hateoas;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;
import vessla.demo.wms.model.Order;
import vessla.demo.wms.web.controller.OrderController;

@Component
public class OrderResourceAssembler implements ResourceAssembler<Order, Resource<Order>> {

    @Override
    public Resource<Order> toResource(Order order) {

        return new Resource<>(order,
                ControllerLinkBuilder.linkTo(methodOn(OrderController.class).generateSingleOrderPdf(order.getId())).withSelfRel());
    }

}
