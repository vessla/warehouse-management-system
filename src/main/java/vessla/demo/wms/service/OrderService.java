package vessla.demo.wms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vessla.demo.wms.model.Order;

public interface OrderService {
	
	Iterable<Order> getOrders();

	Order getOrder(Long id);

	Page<Order> getOrders(Pageable page);

	SaveResult<Order, Order> saveOrder(Order newOrder, boolean override);

	void deleteOrder(Long id);

}
