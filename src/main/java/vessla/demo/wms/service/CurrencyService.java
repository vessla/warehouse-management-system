package vessla.demo.wms.service;

import vessla.demo.wms.model.Currency;

import java.util.List;

public interface CurrencyService {
	
	List<Currency> getCurrencies();

	Currency getCurrency(Integer id);

	SaveResult<Currency, Currency> saveCurrency(Currency newCurrency);

}
