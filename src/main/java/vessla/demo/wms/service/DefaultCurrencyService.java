package vessla.demo.wms.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vessla.demo.wms.dao.CurrencyRepository;
import vessla.demo.wms.model.Currency;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class DefaultCurrencyService implements CurrencyService{

    private static Logger logger = LoggerFactory.getLogger(DefaultCurrencyService.class);

    private final CurrencyRepository currencyRepository;

    @Autowired
    public DefaultCurrencyService(CurrencyRepository currencyRepository){
        this.currencyRepository = currencyRepository;
    }

    @Override
    public List<Currency> getCurrencies() {
        return StreamSupport.stream(currencyRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }

    @Override
    public Currency getCurrency(Integer id) {
        return currencyRepository.findById(id).orElse(null);
    }

    @Override
    public SaveResult<Currency, Currency> saveCurrency(Currency newCurrency) {
        List<Currency> existingCurrencies = currencyRepository.findByName(newCurrency.getName());
        if(existingCurrencies.size()>0){
            //currency already exists - do not try to add anything, just return the existing one as conflicting
            logger.info("[SAVING NEW CURRENCY] Currency "+newCurrency.getName()+" exists - returning this one");
            return new SaveResult<>(null, existingCurrencies.get(0));
        }
        else{
            logger.info("[SAVING NEW DEPARTMENT] Persisting currency: "+newCurrency.getName());
            newCurrency.setId(-1);
            return new SaveResult<>(currencyRepository.save(newCurrency), null);
        }
    }

}
