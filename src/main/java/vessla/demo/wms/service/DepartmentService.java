package vessla.demo.wms.service;

import vessla.demo.wms.model.Department;

import java.util.List;

public interface DepartmentService {
	
	List<Department> getDepartments();

	Department getDepartment(Integer id);

	SaveResult<Department, Department> saveDepartment(Department newDepartment);

}
