package vessla.demo.wms.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vessla.demo.wms.dao.OrderItemRepository;
import vessla.demo.wms.dao.OrderRepository;
import vessla.demo.wms.model.Currency;
import vessla.demo.wms.model.Department;
import vessla.demo.wms.model.Order;
import vessla.demo.wms.model.OrderItem;

@Service
public class DefaultOrderService implements OrderService {

    private static Logger logger = LoggerFactory.getLogger(DefaultOrderService.class);

    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;
    private final CurrencyService currencyService;
    private final DepartmentService departmentService;

    @Autowired
    DefaultOrderService(OrderRepository orderRepository, OrderItemRepository orderItemRepository, CurrencyService currencyService, DepartmentService departmentService) {
        this.orderRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
        this.currencyService = currencyService;
        this.departmentService = departmentService;
    }

    @Override
    public Iterable<Order> getOrders() {
        return orderRepository.findAll();
    }

    public Order getOrder(Long id) { return orderRepository.findById(id).orElse(null); }

    @Override
    public Page<Order> getOrders(Pageable page) {
        return orderRepository.findAllByOrderByIdDesc(page);
    }

    @Override
    public SaveResult<Order, Order> saveOrder(Order newOrder, boolean override) {
        return override?updateExistingOrder(newOrder):saveNewOrder(newOrder);
    }

    @Override
    public void deleteOrder(Long id) {
        orderRepository.deleteById(id);
    }

    private SaveResult<Order, Order> saveNewOrder(Order newOrder){
        //if an order or order item with a specified id already exists, do not override them (return null)
        Order existingOrder = orderRepository.findById(newOrder.getId()).orElse(null);
        if(existingOrder != null){
            //order with the specified id already exists - not allowed to add it as a new order
            logger.info("[SAVING NEW ORDER] order ID exists - returning null");
            return new SaveResult<>(null, existingOrder);
        }
        else {
            OrderItem existingItem = newOrder.getItems().stream().filter(item -> orderItemRepository.existsById(item.getId())).findFirst().orElse(new OrderItem());
            existingItem = orderItemRepository.findById(existingItem.getId()).orElse(null);
            if(existingItem != null){
                //at least one item in the new order has an already existing id (is assigned to another order) - not allowed
                logger.info("[SAVING NEW ORDER] order item ID exists - returning null");
                return new SaveResult<>(null, orderRepository.findById(existingItem.getOrder().getId()).orElse(null));
            }
            else {
                logger.info("[SAVING NEW ORDER] order doesn't exist - attempting to save");
                cascadePersistRelatedEntities(newOrder);
                return new SaveResult<>(orderRepository.save(newOrder), null);
            }
        }
    }

    private SaveResult<Order, Order> updateExistingOrder(Order newOrder){
        if(orderRepository.findById(newOrder.getId()).isPresent()){
            cascadePersistRelatedEntities(newOrder);
            return new SaveResult<>(orderRepository.save(newOrder), null);
        }
        else{
            //if the order does not exists, there is nothing to update (return null)
            return new SaveResult<>(null,null);
        }
    }

    private void cascadePersistRelatedEntities(Order newOrder){
        /* saving required Currency objects (when tried to use cascade=CascadeType.MERGE), Hibernate did not
         * recognize deserialized Currency objects from a HTTP request as same objects, and kept throwing "multiple
         * representations of the same entity" at my face
         */
        newOrder.getItems().forEach(item -> {
            SaveResult<Currency, Currency> saveResult = currencyService.saveCurrency(item.getCurrency());
            item.setCurrency(saveResult.getSavedEntity()!=null?saveResult.getSavedEntity():saveResult.getConflictingEntity());
        });
        // Doing the same thing as mentioned above with the Department object
        SaveResult<Department, Department> saveResult = departmentService.saveDepartment(newOrder.getDepartment());
        newOrder.setDepartment(saveResult.getSavedEntity()!=null?saveResult.getSavedEntity():saveResult.getConflictingEntity());

    }

}
