package vessla.demo.wms.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vessla.demo.wms.dao.DepartmentRepository;
import vessla.demo.wms.model.Department;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class DefaultDepartmentService implements DepartmentService {

    private static Logger logger = LoggerFactory.getLogger(DefaultDepartmentService.class);

    private final DepartmentRepository departmentRepository;

    @Autowired
    DefaultDepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }


    @Override
    public List<Department> getDepartments() {
        return StreamSupport.stream(departmentRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }

    @Override
    public Department getDepartment(Integer id) {
        return departmentRepository.findById(id).orElse(null);
    }

    @Override
    public SaveResult<Department, Department> saveDepartment(Department newDepartment) {
        List<Department> existingDepartments = departmentRepository.findByName(newDepartment.getName());
        if(existingDepartments.size()>0){
            //department already exists - do not try to add anything, just return the existing one
            logger.info("[SAVING NEW DEPARTMENT] Department "+newDepartment.getName()+" exists - returning this one");
            return new SaveResult<>(null, existingDepartments.get(0));
        }
        else{
            logger.info("[SAVING NEW DEPARTMENT] Persisting department: "+newDepartment.getName());
            newDepartment.setId(-1);
            return new SaveResult<>(departmentRepository.save(newDepartment),null);
        }
    }

}
