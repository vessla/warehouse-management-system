package vessla.demo.wms.service;

public class SaveResult<S extends Object, C extends Object> {

    private S savedEntity;

    private C conflictingEntity;

    public SaveResult(S savedEntity, C conflictingEntity){
        this.savedEntity = savedEntity;
        this.conflictingEntity = conflictingEntity;
    }

    public S getSavedEntity() {
        return savedEntity;
    }

    public void setSavedEntity(S savedEntity) {
        this.savedEntity = savedEntity;
    }

    public C getConflictingEntity() {
        return conflictingEntity;
    }

    public void setConflictingEntity(C conflictingEntity) {
        this.conflictingEntity = conflictingEntity;
    }

}
