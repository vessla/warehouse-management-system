package vessla.demo.wms.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="orderitems")
public class OrderItem {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="quantity")
    private int quantity;

    @Column(name="price")
    private float price;

    @NotNull
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="currency_id")
    private Currency currency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    @JsonBackReference //Ignored during serialization process in order to avoid infinite recursion
    private Order order;

    public OrderItem(){
        this(-1L, "default name", -1, 0.0f, new Currency());
    }

    public OrderItem(Long id, String name, int quantity, float price, Currency currency){
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.currency = currency;
    }

    public OrderItem(OrderItem template){
        this(template.getId(), template.getName(), template.getQuantity(), template.getPrice(), template.getCurrency());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
