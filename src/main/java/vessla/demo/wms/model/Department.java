package vessla.demo.wms.model;


import javax.persistence.*;

@Entity
@Table(name="departments")
public class Department {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="name", unique = true)
    private String name;

    public Department(){
        this(-1, "default");
    }

    public Department(String name){
        this(-1, name);
    }

    public Department(Integer id, String name){
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return this.getName()+" ("+this.getId()+")";
    }
}
