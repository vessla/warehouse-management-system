CREATE TABLE currencies (
	id SERIAL NOT NULL,
	name TEXT NOT NULL,
	UNIQUE(name),
	PRIMARY KEY (id)
);

CREATE TABLE departments (
	id SERIAL NOT NULL,
	name TEXT NOT NULL,
	UNIQUE(name),
	PRIMARY KEY (id)
);

CREATE TABLE orderitems (
	id BIGSERIAL NOT NULL,
	name VARCHAR(250) NOT NULL,
	quantity INTEGER NOT NULL,
	price REAL NOT NULL,
	currency_id INTEGER NOT NULL,
	order_id BIGINT,
	PRIMARY KEY (id),
	FOREIGN KEY (currency_id) REFERENCES currencies(id)
);

CREATE TABLE orders (
	id BIGSERIAL NOT NULL,
	creationtime TIMESTAMP NOT NULL,
	description TEXT,
	department_id INTEGER NOT NULL,
	PRIMARY KEY (id)
);

--CREATE TABLE orderitemsorders (
--	id  serial NOT NULL,
--	order_id BIGINT NOT NULL,
--	orderitem_id BIGINT NOT NULL,
--	PRIMARY KEY (id),
--	UNIQUE (order_id, orderitem_id),
--	FOREIGN KEY (order_id) REFERENCES orders (id),
--	FOREIGN KEY (orderitem_id) REFERENCES orderitems (id)
--);