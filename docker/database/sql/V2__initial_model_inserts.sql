INSERT INTO currencies (name) VALUES ('PLN');
INSERT INTO currencies (name) VALUES ('EUR');
INSERT INTO currencies (name) VALUES ('USD');

INSERT INTO departments (name) VALUES ('CNC');

INSERT INTO orders (description, creationTime, department_id) VALUES ('Opis pierwszego zamówienia, który jest zdecydowanie zbyt długim, rozwlekłym, jak również pozbawionym sensu opisem stworzonym jedynie na potrzeby wstępnych testów ;)', TIMESTAMP '2020-10-19 10:23:54', 1);

INSERT INTO orderitems (name, quantity, price, currency_id, order_id) VALUES ('łożyska kulkowe', 2, 15.0, 2, 1);
INSERT INTO orderitems (name, quantity, price, currency_id, order_id) VALUES ('pręt stalowy', 3, 3.0, 1, 1);
INSERT INTO orderitems (name, quantity, price, currency_id, order_id) VALUES ('gulgulator gulgulacyjny generujący szalenie faliste fale gulgulczo-margulcze', 2, 15.0, 2, 1);