## Setup

```
Package the 'wms' jar and copy them to the docker/wms directory (using maven 'clean compile package' -
the jar file should be copied automatically).

Build the application and database docker images as shown below:
docker-compose build
```

## Start

```
docker-compose up -d
```

