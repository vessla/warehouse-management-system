# README #

### What is this repository for? ###

The main goal of this project was to document the skills regarding different technologies and concepts in practice, not to implement a production-ready warehouse management system.
The project consists of several REST endpoints with HATEOAS and resource paging support. The mappings of the HTTP methods to the server-side CRUD operations were inspired by the [Allegro guidelines](https://allegro-restapi-guideline.readthedocs.io/en/latest/Resource/#use-http-methods-to-operate-on-collections-and-entities).

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND

### How do I run it? ###

See the Docker Compose configuration in the [README.MD](https://bitbucket.org/vessla/warehouse-management-system/src/master/docker/README.md) file.

External libraries/tools used in the project:

1. Report generator: [Jasper Reports](https://community.jaspersoft.com/project/jasperreports-library)
2. Database password encryption: [Jasypt](https://github.com/ulisesbocchio/jasypt-spring-boot) 
3. Data storage: [Flyway](https://flywaydb.org/), Hibernate, PostgreSQL
4. Testing: [JUnit](https://junit.org/junit4/), [Traverson](https://docs.spring.io/spring-hateoas/docs/current/api/org/springframework/hateoas/client/Traverson.html)

## Screenshots

### Single order as PDF
![Single order as PDF](screens/wms_orders_single.png)

### List of orders as JSON
![List of orders as JSON](screens/wms_orders_all.png)

### Who do I talk to? ###

pjadamska@gmail.com